﻿using System;

namespace ConsoleApp1
{
    public class Operation
    {
        public static double Add(double firstNumber, double secondNumber)
        {
            var result = firstNumber + secondNumber;
            return result;
        }

        public static double Minus(double firstNumber, double secondNumber)
        {
            var result = firstNumber - secondNumber;
            return result;
        }
        public static double Multiply(double firstNumber, double secondNumber)
        {
            var result = firstNumber * secondNumber;
            return result;
        }
        public static double Divide(double firstNumber, double secondNumber)
        {
            var result = firstNumber / secondNumber;
            return result;
        }
        public static double Sqrt(double firstNumber)
        {
            firstNumber = Math.Sqrt(firstNumber);
            return firstNumber;
        }
        public static double Percent(double firstNumber, double percent)
        {
            double result = 0;
            Arifmetika.ArithmeticActionSelection(firstNumber, firstNumber * (percent / 100), InputConsole.input, ref result);
            return result;
        }
    }
}
