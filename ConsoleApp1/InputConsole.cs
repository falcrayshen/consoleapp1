﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public static class InputConsole
    {
        public static string input = "";
        public static double firstNumber;
        public static double secondNumber;
        public static double percent;

        public static void InputFirstNumber()
        {
            while (!double.TryParse(input, out firstNumber))
            {
                Console.WriteLine("Введите 1-е число: ");
                input = Console.ReadLine();
            }
        }
        public static void InputSecondtNumber()
        {
            while (!double.TryParse(input, out secondNumber))
            {
                Console.WriteLine("Введите 2-е число: ");
                input = Console.ReadLine();
            }
        }
        public static void Percent()
        {
            while (!double.TryParse(input, out percent))
            {
                Console.WriteLine("Введите процент: ");
                input = Console.ReadLine();
            }
        }
        public static string ArythmeticAction(int selection)
        {
            string[] symbols = { "+", "-", "*", "/", "sqrt", "%" };
            bool flagCycle = true;

            while (flagCycle == true)
            {
                if (selection == 0)
                {
                    Console.WriteLine("Выберите арифмитическое действие ( + , - , / , * , % , sqrt)");
                    input = Console.ReadLine();
                }
                else if (selection == 1)
                {
                    Console.WriteLine("Выберите арифмитическое действие с процентом ( + , - , / , * )");
                    input = Console.ReadLine();
                }

                for (int i = 0; i < symbols.Length; i++)
                {
                    if (input == symbols[i])
                    {
                        flagCycle = false;
                        break;
                    }
                }
            }
            return input;
        }
    }
}
