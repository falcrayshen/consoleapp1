﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public static class Calculator
    {
        public static void Calculate()
        {
            string arythmeticSymbol;
            double result = 0;

            InputConsole.InputFirstNumber();
            arythmeticSymbol = InputConsole.ArythmeticAction(0);

            if (arythmeticSymbol == "sqrt")
            {
                Arifmetika.ArithmeticActionSelection(InputConsole.firstNumber, 0, arythmeticSymbol, ref result);
            }
            else if (arythmeticSymbol == "%")
            {
                InputConsole.Percent();
                InputConsole.ArythmeticAction(1);
                Arifmetika.ArithmeticActionSelection(InputConsole.firstNumber, InputConsole.percent, arythmeticSymbol, ref result);
            }
            else  //Операции с двумя введенными числами 
            {
                InputConsole.InputSecondtNumber();
                Arifmetika.ArithmeticActionSelection(InputConsole.firstNumber, InputConsole.secondNumber, arythmeticSymbol, ref result);
            }

            Console.WriteLine($"Результат: {result}");
        }
    }
}
