﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Arifmetika
    {
        public static void ArithmeticActionSelection(double firstNumber, double secondNumber, string operation, ref double result)
        {
            switch (operation)
            {
                case "+":
                    result = Operation.Add(firstNumber, secondNumber);
                    break;
                case "-":
                    result = Operation.Minus(firstNumber, secondNumber);
                    break;
                case "*":
                    result = Operation.Multiply(firstNumber, secondNumber);
                    break;
                case "/":
                    result = Operation.Divide(firstNumber, secondNumber);
                    break;
                case "sqrt":
                    result = Operation.Sqrt(firstNumber);
                    break;
                case "%":
                    result = Operation.Percent(firstNumber, secondNumber);
                    break;
                default:
                    Console.WriteLine("Не верная операция");
                    break;
            }
        }
    }
}
